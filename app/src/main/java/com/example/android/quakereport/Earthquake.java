package com.example.android.quakereport;

/**
 * {@link Earthquake} object contains information related to single earthquake
 */
public class Earthquake {
    /* Magnitude of the earthquake */
    private Double mMag;
    /* Location of the earthquake */
    private String mPlace;
    /* Time in miliseconds of the earthquake */
    private long mTimeInMilliseconds;
    /* Website address of the earthquake */
    private String mUrl;

    /*
    * Earthquake class constructors ne {@link Earthquake} object.
    *
    *  @param mag is the  magnitude of the earthquake
    *  @param place is where the earthquake happened
    *  @param time in miliseconds when the earthquake happened
    *  @param url is the website URL to find more details about the earthquake
    */

    public Earthquake (Double mag, String place, long timeInMilliseconds, String url){
        mMag = mag;
        mPlace = place;
        mTimeInMilliseconds = timeInMilliseconds;
        mUrl = url;
    }

    //Returns the strenght of the earthquake in the value of magnitude
    public Double getMag() { return mMag; }
    //Returns place is where the earthquake happened
    public String getPlace() { return mPlace; }
    //Returns time in miliseconds when the earthquake happened
    public long getTimeInMilliseconds() { return mTimeInMilliseconds; }
    // Returns the website URL to find more information about the earthquake.
    public String getUrl() { return  mUrl; }

}
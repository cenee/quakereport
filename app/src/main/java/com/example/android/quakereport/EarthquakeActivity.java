/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.quakereport;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
//Make sure to use proper loader imports
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;


public class EarthquakeActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<List<Earthquake>>{
    /**
     * Adapter for the list of earthquakes
     */
    private EarthquakeAdapter mAdapter;
    /** Tag for log messages */
    public static final String LOG_TAG = EarthquakeActivity.class.getName();
    /** USGS query url*/
    private static final String USGS_REQUEST_URL =
            "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&orderby=time&minmag=5&limit=10";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.earthquake_activity);

        // Find a reference to the {@link ListView} in the layout
        ListView earthquakeListView = (ListView) findViewById(R.id.list);

        // Create a new adapter that takes an empty list of earthquakes as input
        mAdapter = new EarthquakeAdapter(this, new ArrayList<Earthquake>());
        // Get a reference to the LoaderManager, in order to interact with loaders.
        LoaderManager loaderManager = getSupportLoaderManager();
        // Then initialize the loader. Pass in the int ID, pass in null for the bundle
        // and as a third parameter pass in this activity for the LoaderCallbacks parameter
        // (which is valid because this activity implements the LoaderCallbacks interface).
        loaderManager.initLoader(0,null,this);
        Log.v("Loader INIT","Initialization loader is here.");
        // Set the adapter on the {@link ListView}
        // so the list can be populated in the user interface
        earthquakeListView.setAdapter(mAdapter);


        // Set a click listener to open earthquake website when the list item is clicked on
        earthquakeListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // Find the current earthquake that was clicked on
                Earthquake currentEarthquake = mAdapter.getItem(position);

                // Convert the String URL into a URI object (to pass into the Intent constructor)
                Uri earthquakeUri = Uri.parse(currentEarthquake.getUrl());

                // Create a new intent to view the earthquake URI
                Intent websiteIntent = new Intent(Intent.ACTION_VIEW, earthquakeUri);

                // Send the intent to launch a new activity
                startActivity(websiteIntent);
            }

        });
    }
    /*
     * Loads a list of earthquakes by using an AsyncTask to perform the
     * network request to the given URL.
     */
    private static class EarthquakeLoader extends AsyncTaskLoader<List<Earthquake>> {
        /**
         * Constructs a new {@link EarthquakeLoader}.
         *
         * @param context of the activity
         */
        public EarthquakeLoader(Context context){
            super(context);
        }
        @Override
            protected void onStartLoading() {
                forceLoad();
            Log.v("START-LOAD", "On start loading loader use forceload() method here.");
        }

        @Override
        public List<Earthquake> loadInBackground() {
            Log.v("LOAD-background", "Loader is loading in background here.");
            //Make sure USGS_REQUEST_URL is not null or has an empty value
            if (USGS_REQUEST_URL == null || USGS_REQUEST_URL.isEmpty()) {
                return null;
            }
            // Perform the HTTP (network)request, parse the response, and extract a list of earthquakes.
            List<Earthquake> earthquakes = QueryUtils.fetchEarthquakeData(USGS_REQUEST_URL);
            Log.v("HTTP-request", "Perform network, parse response and extract data from url here.");
            // Return the earthquakes value.
            return earthquakes;
        }

    }
    @Override
    public Loader<List<Earthquake>> onCreateLoader(int i, Bundle bundle) {
        Log.v("CREATE", "Create method of loader is here.");
        // Create and return a new Loader that will take care of creating an Earthquake for given url.
        return new EarthquakeLoader(this);
    }


    @Override
    public void onLoadFinished(Loader<List<Earthquake>> loader, List<Earthquake> earthquakes) {
        Log.v("FINISH", "Finish method of loader is here.");
        // Clear the adapter of previous earthquake data
        mAdapter.clear();
        // If there is a valid list of {@link Earthquake}s, then add them to the adapter's
        // data set. This will trigger the ListView to update.
        if (earthquakes != null && !earthquakes.isEmpty())
            mAdapter.addAll(earthquakes);
    }

    @Override
    public void onLoaderReset(Loader<List<Earthquake>> loader) {
        Log.v("RESET", "Reset method of loader is here.");
        // Make sure to not longer use the adapter
        mAdapter.clear();
    }
}
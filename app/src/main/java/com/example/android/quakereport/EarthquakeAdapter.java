package com.example.android.quakereport;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.graphics.drawable.GradientDrawable;
import java.util.ArrayList;
import java.util.Locale;
import static com.example.android.quakereport.R.id.date;

public class EarthquakeAdapter extends ArrayAdapter<Earthquake> {
    private  Context context;
    public EarthquakeAdapter(Activity context,ArrayList<Earthquake> earthquakes) {
        super(context, 0, earthquakes);
    }
                        //We will be using the split(String string) method in the String class
                        // to split the original string at the position where the text “ of “ occurs.
                        private static final String LOCATION_SEPARATOR = " of ";
    /**
     * Provides a view for an AdapterView (ListView, GridView, etc.)
     *
     * @param position    The position in the list of data that should be displayed in the
     *                    list item view.
     * @param convertView The recycled view to populate.
     * @param parent      The parent ViewGroup that is used for inflation.
     * @return The View for the position in the AdapterView.
     */

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
       // return super.getView(position, convertView, parent);

        // Check if there is an existing view list item (called convertView) that we can reuse,
        // otherwise , if convertView is null, then inflate a new new list_item layout
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }

        // Find the earthquake at the given position in thelist of earthquakes
        final Earthquake currentquake = getItem(position);

        /**
         * Create a PLACE and PLACE_OFFSET info TextViews
         */
        //Get the original location String from the Earthquake object and store that in a variable
        String originalLocation = currentquake.getPlace();
        // Offset Location information of the earthquake
        String location_offset;
        //Primary Location place information
        String location;
        //Website address of the earthquake
        String url = currentquake.getUrl();

        if (originalLocation.contains(LOCATION_SEPARATOR)) {
            String[] parts = originalLocation.split(LOCATION_SEPARATOR);
            location_offset = parts[0] + LOCATION_SEPARATOR;
            location = parts[1];
        } else {
            location_offset = getContext().getString(R.string.near_the);
            location = originalLocation;
        }

        // Find the TextView in the list_item.xml layout with the view ID place
        TextView placeTextView = (TextView) listItemView.findViewById(R.id.place);
        // Display the place location of the currentearthquake in that TextView
        placeTextView.setText(location);

        // Find the TextView in the list_item.xml layout with the view ID place_offset
        TextView placeOffsetTextView = (TextView) listItemView.findViewById(R.id.place_offset);
        // Display the place_offset information of the currentearthquake in that TextView
        placeOffsetTextView.setText(location_offset);

        /**
         * Create MAGNITUDE info TextView
         */
        //Get the original magnitude value
        Double mag = currentquake.getMag();
        // Format the magnitude to show 1 decimal place
        String magFormatted = formatMagnitude(mag);
        // Find the TextView in the list_item.xml layout with the view ID magnitude
        TextView magTextView = (TextView) listItemView.findViewById(R.id.magnitude);
        // Display the magnitude of the current earthquake in that TextView
        magTextView.setText(magFormatted);

        /* * * * * *
         * Create a new TimeInMilliseconds object from the time in milliseconds of the earthquake
         * * * * */
        Date dateObject = new Date(currentquake.getTimeInMilliseconds());
        /**
         * Create a DATE from TimeInMilliseconds dateObject
         */
        // Find the TextView in the list_item.xml layout with the ID date
        TextView dateTextView = (TextView) listItemView.findViewById(date);
        // Format the date string (i.e. "Mar 3, 1984")
        String formattedDate = formatDate(dateObject);
        // Display the date of the current earthquake in that TextView
        dateTextView.setText(formattedDate);
        /**
         * Create a TIME from TimeInMilliseconds dateObject
         */
        // Find the TextView with view ID time
        TextView timeView = (TextView) listItemView.findViewById(R.id.time);
        // Format the time string (i.e. "4:30PM")
        String formattedTime = formatTime(dateObject);
        // Display the time of the current earthquake in that TextView
        timeView.setText(formattedTime);

        // Set the proper background color on the magnitude circle.
        // Fetch the background from the TextView, which is a GradientDrawable.
        GradientDrawable magnitudeCircle = (GradientDrawable) magTextView.getBackground();

        // Get the appropriate background color based on the current earthquake magnitude
        int magnitudeColor = getMagnitudeColor(currentquake.getMag());

        // Set the color on the magnitude circle
        magnitudeCircle.setColor(magnitudeColor);

        // Return the whole list item layout (containing 4 TextViews)
        // that is showing apprpriate data in the ListView
        return listItemView;
    }
    /**
     * Return the formatted date string (i.e. "Mar 3, 1984") from a Date object.
     */
    private String formatDate(Date dateObject) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");
        return dateFormat.format(dateObject);
    }
    /**
     * Return the formatted date string (i.e. "4:30 PM") from a Date object.
     */
    private String formatTime(Date dateObject) {
        SimpleDateFormat timeFormat = new SimpleDateFormat("h:mm a");
        return timeFormat.format(dateObject);
    }

    /**
     * Return the formatted magnitude string showing 1 decimal place (i.e. "3.2")
     * from a decimal magnitude value.
     */
    private String formatMagnitude(double magnitude) {
        //set magnitude decimal format with 1 decimal number
        // and use dot as decimal seperator - Locale.US decimal format symbol
        DecimalFormat magnitudeFormat = new DecimalFormat("0.0", new DecimalFormatSymbols(Locale.US));
        return magnitudeFormat.format(magnitude);
    }
    //Returns the correct color value based on the current earthquake’s magnitude value.
    private int getMagnitudeColor(double magnitude){
        int magnitudeColorRes;
        magnitude = (int) Math.floor(magnitude);
        switch ((int) magnitude){
            case 0:
            case 1:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude1);
                break;
            case 2:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude2);
                break;
            case 3:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude3);
                break;
            case 4:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude4);
                break;
            case 5:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude5);
                break;
            case 6:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude6);
                break;
            case 7:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude7);
                break;
            case 8:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude8);
                break;
            case 9:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude9);
                break;
            case 10:
            default:
                magnitudeColorRes = ContextCompat.getColor(getContext(), R.color.magnitude10plus);
                break;
        }
    return magnitudeColorRes;
    }

}